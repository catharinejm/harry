#![feature(box_syntax, box_patterns, type_ascription)]

extern crate harry;

use harry::process::*;

use std::any::Any;

fn main() {
    println!("{}", MSG);
    let mut exec = Executor::new();
    let env = exec.new_env(vec![]);
    let _proc = exec.spawn(env, handler);
    while exec.step() {}
}

struct Envelope {
    msg: Box<Any>,
    sender: PID,
}

fn handler(env: Env) -> ProcessState {
    // println!("handler called with {} parameters", env.parameters().len());
    let sub_env = env.executor().empty_env();
    let new_proc = env.executor().spawn(sub_env, recv_loop);
    env.executor().send(
        new_proc,
        Envelope {
            msg: box "word up",
            sender: env.my_pid(),
        },
    );
    ProcessState::Receive(
        env,
        vec![(
            box |_env, msg| {
                if msg.is::<&str>() {
                    // println!("****** IS STRING");
                    MatchResult::Match
                } else {
                    // println!("****** IS ==NOT== STRING");
                    MatchResult::NoMatch
                }
            },
            handle_recv_str,
        )],
    )
}

fn handle_recv_str(mut env: Env) -> ProcessState {
    let msg = env.parameters_mut().pop().unwrap();
    let string = msg.downcast_ref::<&str>().unwrap();
    println!("Response: {}", string);
    ProcessState::Done(env)
}

fn recv_loop(env: Env) -> ProcessState {
    ProcessState::Receive(
        env,
        vec![(
            box |_env, msg| {
                if msg.is::<Envelope>() {
                    // println!("****** IS ENVELOPE");
                    MatchResult::Match
                } else {
                    // println!("****** IS ==NOT== ENVELOPE");
                    MatchResult::NoMatch
                }
            },
            recv_loop_envelope,
        )],
    )
}

fn recv_loop_envelope(mut env: Env) -> ProcessState {
    let envelope = env
        .parameters_mut()
        .pop()
        .unwrap()
        .downcast::<Envelope>()
        .unwrap();
    env.executor().send(envelope.sender, "k");
    recv_loop(env)
}

use std::any::Any;
use std::collections::{HashMap, HashSet, VecDeque};
use std::fmt::{self, Debug, Formatter};

pub const MSG: &'static str = "Guten Tag, Welt!";

#[derive(Debug)]
pub struct Executor {
    proc_map: HashMap<PID, Box<Process>>,
    run_queue: VecDeque<(PID, *mut Process)>,
    wait_map: HashMap<PID, ReceiveState>,
    dead_proc_addrs: HashSet<PID>,
    // blocked: VecDeque<*mut Process>,
    next_pid: PID,
    // cycles_since_last_action: usize,
    // persistent_ptr: *const Process,
}

impl Executor {
    pub fn new() -> Executor {
        Executor {
            proc_map: HashMap::new(),
            run_queue: VecDeque::new(),
            wait_map: HashMap::new(),
            dead_proc_addrs: HashSet::new(),
            // blocked: VecDeque::new(),
            next_pid: PID(0),
            // cycles_since_last_action: 0,
            // persistent_ptr: ptr::null(),
        }
    }

    pub fn new_env(&mut self, params: Vec<Box<Any>>) -> Env {
        Env {
            executor: self,
            parameters: params,
            my_pid: PID(0),
        }
    }

    pub fn empty_env(&mut self) -> Env {
        self.new_env(vec![])
    }

    pub fn next_pid(&mut self) -> PID {
        let pid = self.next_pid;
        self.next_pid = PID(self.next_pid.0 + 1);
        pid
    }

    pub fn spawn(&mut self, mut env: Env, func: fn(Env) -> ProcessState) -> PID {
        let pid = self.next_pid();
        env.my_pid = pid;
        let mut proc = box Process::new(pid, env, func);
        self.run_queue.push_back((pid, proc.as_mut()));
        self.proc_map.insert(pid, proc);
        pid
    }

    pub fn kill(&mut self, pid: PID) {
        self.wait_map.remove(&pid);
        self.proc_map.remove(&pid);
        if self.run_queue.len() > 0 {
            for i in 0..self.run_queue.len() {
                if self.run_queue[i].0 == pid {
                    self.run_queue.remove(i);
                }
            }
        }
        println!(
            "Killed {:?}, remaining procs: {}",
            pid,
            self.run_queue.len()
        );
    }

    pub fn send<T>(&mut self, pid: PID, msg: T)
    where
        T: Any + Sized + 'static,
    {
        match self.proc_map.get_mut(&pid) {
            Some(box proc) => {
                proc.mbox.push(Msg::new(msg));
                if let Some(waiter) = self.wait_map.remove(&pid) {
                    proc.set_run_state(RunState::ReceiveState(waiter));
                    self.run_queue.push_back((pid, proc));
                }
            }
            None => println!("No such pid: {:?} (send)", pid),
        }
    }

    fn receive(&mut self, mut receive_state: ReceiveState) {
        let proc = receive_state.process();
        proc.set_run_state(RunState::ReceiveState(receive_state));
        self.run_queue.push_back((proc.pid, proc));
    }

    pub fn step(&mut self) -> bool {
        match self.run_queue.pop_front() {
            Some((pid, _)) if self.dead_proc_addrs.contains(&pid) => {
                println!("{:?} is dead, skipping...", pid);
                self.step();
            }
            Some((_, proc_mut_p)) => {
                let proc = unsafe { &mut *proc_mut_p };
                // println!("Running {:?}", proc);
                match proc.run_state.take() {
                    Some(st @ RunState::RunState { .. }) => {
                        // println!("\t{:?} in RunState", proc);
                        if let RunState::RunState { env, func, .. } = st {
                            match func(env) {
                                ProcessState::Done(_) => self.kill(proc.pid),
                                ProcessState::Receive(new_env, matchers) => {
                                    let receive_state = ReceiveState {
                                        process: proc_mut_p,
                                        scan_index: 0,
                                        env: new_env,
                                        matchers,
                                    };
                                    self.receive(receive_state);
                                }
                            }
                        }
                    }
                    Some(RunState::ReceiveState(mut st)) => {
                        // println!("\t{:?} in ReceiveState", proc);
                        let mut match_idx = -1isize;
                        let mut scan_count = 0usize;
                        {
                            let mut mbox_iter = proc.mbox.iter_mut().skip(st.scan_index);
                            'mbox_loop: for msg in mbox_iter {
                                for (idx, (matcher, _)) in st.matchers.iter().enumerate() {
                                    match matcher(&st.env, msg) {
                                        MatchResult::Match => {
                                            match_idx = idx as isize;
                                            break 'mbox_loop;
                                        }
                                        MatchResult::NoMatch => (),
                                    }
                                }
                                scan_count += 1;
                            }
                        }
                        if match_idx >= 0 {
                            let msg = proc.mbox.remove(scan_count);
                            proc.set_run_state(RunState::RunState {
                                process: proc_mut_p,
                                env: st.env.push_param(msg.0),
                                func: st.matchers[match_idx as usize].1,
                            });
                            self.run_queue.push_back((proc.pid, proc_mut_p));
                        } else {
                            st.scan_index += scan_count;
                            self.wait_map.insert(proc.pid, st);
                        }
                    }
                    None => (),
                }
            }
            _ => (),
        }
        let ret = self.run_queue.len() > 0;
        // println!("Returning {} from step()", ret);
        ret
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct PID(usize);

pub struct Process {
    pid: PID,
    run_state: Option<RunState>,
    mbox: Vec<Msg>,
}

impl Process {
    pub fn new(pid: PID, env: Env, func: fn(Env) -> ProcessState) -> Process {
        let mut proc = Process {
            pid: pid,
            run_state: None,
            mbox: vec![],
        };
        let run_state = RunState::RunState {
            process: &mut proc,
            env,
            func,
        };
        proc.set_run_state(run_state);
        proc
    }

    fn set_run_state(&mut self, run_state: RunState) {
        match self.run_state {
            Some(_) => panic!("Continuation already set! {:?}", self),
            None => self.run_state = Some(run_state),
        }
    }
}

impl Debug for Process {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        write!(f, "Process<{:?}>", self.pid)
    }
}

#[derive(Debug)]
pub struct Env {
    executor: *mut Executor,
    my_pid: PID,
    parameters: Vec<Box<Any>>,
}

impl Env {
    pub fn executor(&self) -> &mut Executor {
        unsafe { &mut *self.executor }
    }

    pub fn parameters(&self) -> &Vec<Box<Any>> {
        &self.parameters
    }

    pub fn parameters_mut(&mut self) -> &mut Vec<Box<Any>> {
        &mut self.parameters
    }

    pub fn my_pid(&self) -> PID {
        self.my_pid
    }

    pub fn with_params(&self, params: Vec<Box<Any>>) -> Env {
        Env {
            executor: self.executor,
            my_pid: self.my_pid,
            parameters: params,
        }
    }

    pub fn push_param(mut self, param: Box<Any>) -> Env {
        self.parameters.push(param);
        self
    }
}

struct ReceiveState {
    process: *mut Process,
    scan_index: usize,
    env: Env,
    matchers: Vec<(MatchFn, Continuation)>,
}

impl ReceiveState {
    fn process(&mut self) -> &'static mut Process {
        unsafe { &mut *self.process }
    }
}

impl Debug for ReceiveState {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "ReceiveState {{ process: {:?}, scan_index: {:?}, env: {:?}, matchers: [..] }}",
            unsafe { &*self.process },
            self.scan_index,
            self.env
        )
    }
}

enum RunState {
    RunState {
        process: *mut Process,
        env: Env,
        func: fn(Env) -> ProcessState,
    },
    ReceiveState(ReceiveState),
}

impl Debug for RunState {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        match self {
            RunState::RunState { process, env, .. } => write!(
                f,
                "RunState {{ process: {:?}, env: {:?}, func: Fn(Env) -> Env }}",
                unsafe { &**process },
                env
            ),
            RunState::ReceiveState(st) => write!(f, "{:?}", st),
        }
    }
}

type MatchFn = Box<Fn(&Env, &Msg) -> MatchResult>;
type Continuation = fn(Env) -> ProcessState;

pub enum ProcessState {
    Done(Env),
    Receive(Env, Vec<(MatchFn, Continuation)>),
}

pub enum MatchResult {
    Match,
    NoMatch,
}

impl Debug for ProcessState {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        match self {
            ProcessState::Done(env) => write!(f, "Done({:?})", env),
            ProcessState::Receive(env, _) => write!(f, "Receive({:?}, Fn(..))", env),
            // ProcessState::ReceiveNoMatch(env) => write!(f, "ReceiveNoMatch({:?})", env),
            // ProcessState::Recurse(env) => write!(f, "Recurse({:?})", env),
        }
    }
}

#[derive(Debug)]
pub struct Msg(Box<Any + 'static>);

impl Msg {
    pub fn new<T>(t: T) -> Msg
    where
        T: Any + Sized + 'static,
    {
        Msg(box t)
    }

    pub fn downcast<'a, T>(&'a self) -> Option<&T>
    where
        T: 'static,
    {
        self.0.downcast_ref::<T>()
    }

    pub fn is<'a, T>(&'a self) -> bool
    where
        T: 'static,
    {
        self.0.is::<T>()
    }

    pub fn for_<T, F>(&self, fun: F) -> bool
    where
        T: 'static + Sized,
        F: Fn(&T),
    {
        if let Some(t) = self.0.downcast_ref::<T>() {
            fun(&t);
            true
        } else {
            false
        }
    }
}
